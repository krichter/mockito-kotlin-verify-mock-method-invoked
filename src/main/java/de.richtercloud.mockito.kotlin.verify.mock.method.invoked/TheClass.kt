package de.richtercloud.mockito.kotlin.verify.mock.method.invoked

import org.springframework.beans.factory.annotation.Autowired

class TheClass constructor(@Autowired val someService: SomeService){

    fun someMethod(arg: String) {
        someService.someMethod(arg)
    }
}
