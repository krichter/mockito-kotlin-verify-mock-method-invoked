package de.richtercloud.mockito.kotlin.verify.mock.method.invoked

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
@Suppress("UtilityClassWithPublicConstructor")
internal open class TheApplication {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(TheApplication::class.java, *args)
        }
    }
}
