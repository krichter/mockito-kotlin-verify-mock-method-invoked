package de.richtercloud.mockito.kotlin.verify.mock.method.invoked

import org.springframework.stereotype.Service

@Service
open class SomeService {

    fun someMethod(arg: String) {
        println("arg: $arg")
        if(arg.isBlank()) {
            throw IllegalArgumentException("arg mustn't be blank")
        }
    }
}
