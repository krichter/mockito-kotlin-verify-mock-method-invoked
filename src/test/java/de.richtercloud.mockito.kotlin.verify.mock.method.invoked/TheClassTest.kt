package de.richtercloud.mockito.kotlin.verify.mock.method.invoked

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.reset
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.InjectMocks
import org.mockito.Mock
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
class TheClassTest {

    @Test
    fun testSomeMethodArgumentMatcher() {
        val someService: SomeService = mock()
        val instance: TheClass = TheClass(someService)
        instance.someMethod("not blank")

        verify(someService).someMethod(anyString())
    }

    @Test
    fun testSomeMethodUnwantedArgument() {
        val someService: SomeService = mock()
        val instance: TheClass = TheClass(someService)
        instance.someMethod("not blank")

        verify(someService).someMethod("")
    }

    @Test
    fun testSomeMethodUnwantedArgumentNotBlank() {
        val someService: SomeService = mock()
        val instance: TheClass = TheClass(someService)
        instance.someMethod("not blank")

        verify(someService).someMethod("not blank")
    }
}
